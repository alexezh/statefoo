#pragma once

#include <functional>
#include <vector>
#include "span.h"
#include "string_span.h"
#include "BondyException.h"
#include "jsonreader.h"
#include "jsonwriter.h"

namespace Bondy
{

using StringView = gsl::string_span<>;
using ConstStringView = gsl::cstring_span<>;

class ContextBase;

// An application accesses model object via Ptr classes derived from BasePtr
template <typename T>
class BasePtr
{
public:
	using IsPtr = std::true_type;

	T* Ptr()
	{
		return m_Ptr;
	}

	T& Ref()
	{
		return *m_Ptr;
	}

	const T* Ptr() const
	{
		return m_Ptr;
	}

	const T& Ref() const
	{
		return *m_Ptr;
	}

protected:
	BasePtr(ContextBase& ctx, T* p)
		: m_Ctx(&ctx)
		, m_Ptr(p)
	{
	}

	ContextBase* m_Ctx { nullptr };
	T* m_Ptr { nullptr };
};

using Blob = gsl::span<uint8_t>;

// represents an span of objects owned by context (wrapper over span)
class BlobPtr
{
public:
    BlobPtr(ContextBase& ctx, gsl::span<uint8_t> data)
        : m_Context(&ctx)
        , m_Data(data)
    {
    }

	gsl::span<uint8_t> Data()
	{
		return m_Data;
	}

private:
    gsl::span<uint8_t> m_Data;
    ContextBase* m_Context;
};

// represents an string span owned by context (wrapper over string span)
class StringPtr
{
public:
    StringPtr(ContextBase& ctx, StringView view)
        : m_Context(&ctx)
        , m_View(view)
    {
    }
    
    StringView Data()
    {
        return m_View;
    }
private:
    StringView m_View;
    ContextBase* m_Context;
};


class ContextBase : public std::enable_shared_from_this<ContextBase>
{
public:
    // stores data from vector so it can be safely referred by model
    // returns view to stored data
    ConstStringView Store(std::vector<char>&& val)
    {
        m_External.push_back(std::move(val));
        return ConstStringView(m_External.back().data(), m_External.back().size());
    }
    
    StringPtr AllocString(StringView view);
    StringPtr AllocString(ConstStringView view);
    
    template <class T>
    typename T::Ptr Alloc();
    
private:
    gsl::span<uint8_t> AllocBlock(size_t cb);
    
private:
    std::vector<std::vector<char>> m_External;

	uint32_t m_cbTotal { 0 };
	struct Block
	{
		uint32_t Total { 0 };
		uint32_t Used { 0 };
		uint8_t Data[1];
	};
    std::vector<std::unique_ptr<Block>> m_Alloc;
};

template <class T>
class Context : public ContextBase
{
public:
    using RootPtr = typename T::Ptr;
    
    RootPtr Root()
    {
        return RootPtr(*this, &m_Root);
    }
    
private:
    T m_Root;
};


// generic serialization support
template <class T, typename isPtr>
struct JsonSerializer
{
    static void Serialize(ContextBase& ctx, JsonWriter& wrt, const T& val)
    {
        static_assert(false, "should not be called");
    }
    
    static void Deserialize(ContextBase& ctx, JsonReader& rdr, T& val)
    {
        static_assert(false, "should not be called");
    }

};

// Specialization for classes derived from BasePtr<T>
template <class T>
struct JsonSerializer<T, std::true_type>
{
    static void Serialize(Bondy::ContextBase& ctx, JsonWriter& wrt, const T& val)
    {
        JsonSerializer<typename T::Obj, typename T::Obj::IsPtr>::Serialize(ctx, wrt, val.Ref());
    }

    static void Deserialize(ContextBase& ctx, JsonReader& rdr, T& val)
    {
        JsonSerializer<typename T::Obj, typename T::Obj::IsPtr>::Deserialize(ctx, rdr, val.Ref)
    }
};

template <class T>
void JsonSerialize(Bondy::ContextBase& ctx, JsonWriter& wrt, const T& val)
{
    JsonSerializer<T, typename T::IsPtr>::Serialize(ctx, wrt, val);
}

template <class T>
void JsonDeserialize(ContextBase& ctx, JsonReader& rdr, T& val)
{
    JsonSerializer<T, typename T::IsPtr>::Deserialize(ctx, rdr, val);
}

template <>
inline void JsonSerialize<uint32_t>(Bondy::ContextBase& ctx, JsonWriter& wrt, const uint32_t& val)
{
    wrt.WriteIntValue(val);
}

template <>
inline void JsonDeserialize<uint32_t>(ContextBase& ctx, JsonReader& rdr, uint32_t& val)
{
    val = rdr.IntValue();
}

template <class T>
void JsonDeserialize(ContextBase& ctx, JsonReader&& rdr, T& val)
{
    // we do not have to move rdr to deserialize function; all we need is for rdr to die
    // at the end of the function
    JsonSerializer<T, typename T::IsPtr>::Deserialize(ctx, rdr, val);
}

template <class T>
void JsonDeserialize(ContextBase& ctx, BasePtr<T>& val, std::vector<char>&& data)
{
    auto wrtRes = ctx.Store(std::move(data));
    Bondy::JsonReader rdr(wrtRes);

	// do the first read to initialize. We expect to read something not empty
	if (!rdr.ReadNext())
		Exception::Throw();

    Bondy::JsonDeserialize(ctx, rdr, val.Ref());
}

// bonded skips through json object and records the position
template <typename T>
class Bonded
{
    template <typename U, typename isPtr>
    friend struct JsonSerializer;
    
public:
    using IsPtr = std::false_type;

	Bonded()
    {
    }
    
    void Serialize(ContextBase& ctx, const T& val)
    {
        JsonWriter wrt;
        ::Bondy::JsonSerialize(ctx, wrt, val);
        m_Content = ctx.Store(wrt.TakeResult());
    }
    
    void Deserialize(ContextBase& ctx, T& val)
    {
        JsonReader rdr(m_Content);

		// we expect bonded object to contain something
		if (!rdr.ReadNext())
			Exception::Throw();

        ::Bondy::JsonDeserialize(ctx, rdr, val);
    }
    
private:
    ConstStringView m_Content;
};

template <typename T>
class BondedPtr : public BasePtr<Bonded<T>>
{
public:
    using Obj = T;
    using Super = BasePtr<Bonded<T>>;
    
    BondedPtr(ContextBase& ctx, Bonded<T>* p)
        : BasePtr<Bonded<T>>(ctx, p)
    {
    }

    void Serialize(const BasePtr<T>& val)
    {
        Super::Ptr()->Serialize(*Super::m_Ctx, val.Ref());
    }
    
    void Deserialize(BasePtr<T>& val)
    {
        Super::Ptr()->Deserialize(*Super::m_Ctx, val.Ref());
    }
};

template <class T>
class JsonSerializer<Bonded<T>, std::false_type>
{
public:
    static void Serialize(ContextBase& ctx, JsonWriter& wrt, const Bonded<T>& val)
    {
        wrt.WriteRawValue(val.m_Content);
    }

    static void Deserialize(ContextBase& ctx, JsonReader& rdr, Bonded<T>& val)
    {
        // we are at object; skip it and record position before and after
        assert(rdr.Type() == JsonReader::NodeType::Object);
        
        auto objStart = rdr.CurrentPos();
        rdr.ReadNext();
        assert(rdr.Type() == JsonReader::NodeType::EndObject);
        auto objEnd = rdr.CurrentPos();
        
        val.m_Content = ConstStringView(objStart, objEnd - objStart);
    }
};

// special wrapper for vector
template <class T>
class VectorPtr
{
public:
    using TPtr = typename T::Ptr;
    using storage = std::vector<T>;
    using iterator = storage;
    
    VectorPtr(ContextBase& ctx, std::vector<T>& data)
        : m_Ctx(&ctx)
        , m_Data(&data)
    {
    }
    
    void push_back(const T& p)
    {
        m_Data->push_back(p);
    }

	size_t size() const
	{
		return m_Data->size();
	}
    
    TPtr at(size_t idx)
    {
        return TPtr(m_Ctx, m_Data->at(idx));
    }
    
private:
    std::vector<T>* m_Data;
    ContextBase* m_Ctx;
};

// Vector specialization for StringView
template <>
class VectorPtr<StringView>
{
public:
    VectorPtr(ContextBase& ctx, std::vector<StringView>& data)
        : m_Ctx(&ctx)
        , m_Data(&data)
    {
    }

    void push_back(StringView p)
    {
        m_Data->push_back(p);
    }

    void push_back(StringPtr p)
    {
        m_Data->push_back(p.Data());
    }
    
    StringPtr at(size_t idx)
    {
        return StringPtr(*m_Ctx, m_Data->at(idx));
    }

private:
    std::vector<StringView>* m_Data;
    ContextBase* m_Ctx;
};

template <class T>
class StackObj : public T::Ptr
{
public:
    StackObj(ContextBase& ctx)
        : T::Ptr(ctx, &m_Obj)
    {
    }
    
private:
    T m_Obj;
};

} // Bondy
