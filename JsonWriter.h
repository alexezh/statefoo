// Copyright (c) 2013 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <string>
#include <vector>
#include "string_span.h"

namespace Bondy {

using JsonStringSpan = gsl::cstring_span<>;

// Writes a JSON (RFC 4627) encoded value to an array, one token at a time.
// The stream includes both literal values (strings, numbers, booleans and nulls) as well as the begin and
// end delimiters of objects and array
class JsonWriter
{
public:
	JsonWriter()
		: _AddSep(false)
		, _Ident(0)
	{
	}
	
    // write "name =" part of pair
    JsonWriter& WriteName(JsonStringSpan name)
    {
		BeforeValue();
        WriteNameWorker(name.data(), name.length());
        return *this;
    }
    
    JsonWriter& WriteRawValue(JsonStringSpan data)
    {
		_Result.insert(_Result.end(), data.begin(), data.end());
		AfterValue();
        return *this;
    }

    // write (name, string) pair
	JsonWriter& WriteStringPair(JsonStringSpan name, JsonStringSpan val)
	{
		return WriteStringValueWorker(name.data(), name.length(), val.data(), val.length());
	}

    // write string value
	JsonWriter& WriteStringValue(JsonStringSpan val)
	{
		return WriteStringValueWorker(nullptr, 0, val.data(), val.length());
	}

    // write (name, int) pair
	JsonWriter& WriteIntPair(JsonStringSpan name, int val)
	{
		return WriteIntValueWorker(name.data(), name.length(), val);
	}

    // write int value
	JsonWriter& WriteIntValue(int val)
	{
		return WriteIntValueWorker(nullptr, 0, val);
	}

    // write (name, int64) pair
	JsonWriter& WriteInt64Value(int64_t val)
	{
		return WriteInt64ValueWorker(nullptr, 0, val);
	}

    // write int64 value
	JsonWriter& WriteInt64Value(JsonStringSpan name, int64_t val)
	{
		return WriteInt64ValueWorker(name.data(), name.length(), val);
	}

    // start writing (name, object) pair; write initial {
    // caller should call WriteObjectValueEnd to complete object
	JsonWriter& WriteObjectPairStart(JsonStringSpan name)
	{
		return WriteObjectValueStartWorker(name.data(), name.length());
	}

    // start writing object value; write initial {
	JsonWriter& WriteObjectValueStart()
	{
		return WriteObjectValueStartWorker(nullptr, 0);
	}

    // finish writing object value; write final }
	JsonWriter& WriteObjectValueEnd()
	{
		return WriteObjectValueEndWorker();
	}
    
    // start writing (name, array) pair; write initial [
    // caller should call WriteArrayValueEnd to complete object
	JsonWriter& WriteArrayPairStart(JsonStringSpan name)
	{
		return WriteArrayValueStartWorker(name.data(), name.length());
	}

    // start writing object value; write initial [
	JsonWriter& WriteArrayValueStart()
	{
		return WriteArrayValueStartWorker(nullptr, 0);
	}

    // finish writing array value; write final ]
	JsonWriter& WriteArrayValueEnd()
	{
		return WriteArrayValueEndWorker();
	}

    // return generated json
	const std::vector<char> & Result() const
    {
        return _Result;
    }

    // take ownership of generated json
	std::vector<char> TakeResult()
    {
        return std::move(_Result);
    }

private:
	void BeforeValue()
	{
		if (_AddSep)
		{
			_Result.push_back(',');
			_AddSep = false;
		}
	}
	void AfterValue()
	{
		_AddSep = true;
	}

	JsonWriter& WriteStringValueWorker(const char* pszName, size_t cchName, const char* pszValue, size_t cchValue)
	{
		BeforeValue();
		if (pszName)
			WriteNameWorker(pszName, cchName);
		WriteRawString(pszValue, cchValue);

		AfterValue();
		return *this;
	}

	JsonWriter& WriteIntValueWorker(const char* pszName, size_t cchName, int val)
	{
		BeforeValue();
		if (pszName)
			WriteNameWorker(pszName, cchName);

		// convert int to string and append
		std::array<char, 32> arVal;
		auto cchVal = i2a(val, arVal);
		_Result.insert(_Result.end(), arVal.data(), arVal.data() + cchVal);

		AfterValue();
		return *this;
	}

	JsonWriter& WriteInt64ValueWorker(const char* pszName, size_t cchName, int64_t val)
	{
		BeforeValue();
		if (pszName)
			WriteNameWorker(pszName, cchName);
		// convert int to string and append
		std::array<char, 64> arVal;
		auto cchVal = i2a(val, arVal);
		_Result.insert(_Result.end(), arVal.data(), arVal.data() + cchVal);

		AfterValue();
		return *this;
	}

	JsonWriter& WriteObjectValueStartWorker(const char* pszName, size_t cchName)
	{
		BeforeValue();
		if (pszName)
			WriteNameWorker(pszName, cchName);

		_Result.push_back('\n');
		_Result.push_back('{');
		_Ident++;

		return *this;
	}

	JsonWriter& WriteObjectValueEndWorker()
	{
		_Ident--;
		_Result.push_back('}');

		AfterValue();
		return *this;
	}

	JsonWriter& WriteArrayValueStartWorker(const char* pszName, size_t cchName)
	{
		BeforeValue();
		if (pszName)
			WriteNameWorker(pszName, cchName);

		_Result.push_back('[');
		return *this;
	}

	JsonWriter& WriteArrayValueEndWorker()
	{
		_Result.push_back(']');

		AfterValue();
		return *this;
	}

	void WriteNameWorker(const char* pszName, size_t cchName)
	{

		WriteRawString(pszName, cchName);
		_Result.push_back(':');
	}

	void WriteRawString(const char * pszVal, size_t cchVal)
	{
		_Result.push_back('"');
		for(;*pszVal && cchVal > 0;pszVal++, cchVal--)
		{
            char c = *pszVal;
			// escape
            switch(c)
            {
                case '"': 
                case '\\': _Result.push_back('\\'); _Result.push_back(c); break;
                case '\b': _Result.push_back('\\'); _Result.push_back('b'); break;
                case '\f': _Result.push_back('\\'); _Result.push_back('f'); break;
                case '\n': _Result.push_back('\\'); _Result.push_back('n'); break;
                case '\r': _Result.push_back('\\'); _Result.push_back('r'); break;
                case '\t': _Result.push_back('\\'); _Result.push_back('t'); break;
                default:
			        _Result.push_back(*pszVal);
                    break;
            }
		}
		_Result.push_back('"');
	}

    template <typename TArr>
    static size_t i2a(int64_t n, TArr& buffer)
    {
        int sign = 1;
        size_t maxLen = buffer.size() - 1;

        if (n < 0)
        {
            sign = 1;
            n = -n;
        }
        
        int i = 0;
        do
        {
            if(i >= maxLen)
                std::terminate();
            
            buffer[i++] = n % 10 + '0';
        } while ((n /= 10) > 0);
        
        if (sign < 0)
        {
            if(i >= maxLen)
                std::terminate();

            buffer[i++] = '-';
        }
        buffer[i] = '\0';
        
        int k, j;
        char c;

        for (k = 0, j = i-1; k<j; k++, j--)
        {
            c = buffer[k];
            buffer[k] = buffer[j];
            buffer[j] = c;
        }
        
        return i;
    }

	std::vector<char> _Result;
	int _Ident;
	std::vector<char> _IdentBuf;
	bool _AddSep;
};

}
