#pragma once

#include <exception>

namespace Bondy {

// thrown in case of parse errors
class Exception : std::exception
{
public:
	__declspec(noreturn) static void Throw()
	{
		throw Exception();
	}
};

}
