#include "bondy.h"

namespace Bondy
{

gsl::span<uint8_t> ContextBase::AllocBlock(size_t cb)
{
	for (auto& block : m_Alloc)
	{
		if (block->Used + cb <= block->Total)
		{
			uint8_t* start = block->Data;
			block->Used += cb;
			m_cbTotal += cb;
			return gsl::span<uint8_t>(start, cb);
		}
	}

	uint32_t blockSize = (m_cbTotal == 0) ? 16 * 1024 : m_cbTotal;
	blockSize = std::max<uint32_t>(cb, blockSize);
	std::unique_ptr<uint8_t[]> block(new uint8_t[blockSize + sizeof(Block) - 1]);

	m_Alloc.push_back(std::unique_ptr<Block>(reinterpret_cast<Block*>(block.release())));
	m_Alloc.back()->Used += cb;
	m_cbTotal += cb;
	return gsl::span<uint8_t>(&m_Alloc.back()->Data[0], cb);
}

StringPtr ContextBase::AllocString(StringView view)
{
	auto block = AllocBlock(view.size());
	memcpy_s(block.data(), block.size(), view.data(), view.size());
    return StringPtr(*this, gsl::string_span<>(reinterpret_cast<char*>(block.data()), block.size()));
}

StringPtr ContextBase::AllocString(ConstStringView view)
{
	auto block = AllocBlock(view.size());
	memcpy_s(block.data(), block.size(), view.data(), view.size());
	return StringPtr(*this, gsl::string_span<>(reinterpret_cast<char*>(block.data()), block.size()));
}
    
}