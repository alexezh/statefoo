// Copyright (c) 2013 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <memory>
#include <vector>
#include <string>
#include <assert.h>
#include "BondyException.h"
#include "string_span.h"

namespace Bondy {

using JsonStringSpan = gsl::cstring_span<>;

// reads json stream one element at a time
// the object is shared between multiple reader classes so we use refcounting
// since objects are single threaded, shared_ptr is overkill so we use custom ref
class JsonElementReader
{
public:
	enum class NodeType
	{
		None,
		String,
		Int,
		Array,
		EndArray,
		Object,
		EndObject,
	};

public:
	JsonElementReader(JsonStringSpan data)
	{
		InitState(data.data(), data.size());
	}

	void AddRef()
	{
		++m_cRef;
	}
	void Release()
	{
		--m_cRef;
		if (m_cRef == 0)
			delete this;
	}

	bool ReadNext();

	bool IsEnd() { return _ReachedEnd; }
	NodeType Type() { return _Type; }
    bool IsString() { return _Type == NodeType::String; }
	const std::string& Name() { return _Name; }
	const std::string& Value() { return _Value; }
	int IntValue() { return (int)_IntValue; }
	int64_t Int64Value() { return _IntValue; }

	void SetSkipMode(bool val)
	{
		_Skip = val;
	}

    const char* CurrentPos() { return _pszCur; }

private:

	bool IsWhite(char c)
	{
		return (c == ' ' || c == '\n');
	}

    /*
        Following chars are escaped
        \"
        \\
        \/
        \b
        \f
        \n
        \r
        \t
        \u four-hex-digits
    */

	// returns current char and moves cursor to next position
    char ReadChar()
	{
		if(_pszCur < _pszEnd)
		{
			char c = *_pszCur;
			_pszCur++;
			return c;
		}
		else
		{
			return '\0';
		}
    }
    char ReadUnescape()
	{
        char c = ReadChar();
        switch(c)
        {
            case '"': 
            case '\\': return c;
            case 'b': return '\b';
            case 'f': return '\f';
            case 'n': return '\n';
            case 'r': return '\r';
            case 't': return '\t';
            default:
			    Exception::Throw();
                return '\0';
        }
	}

	// returns current char without reading it
	char PeekChar()
	{
		if(_pszCur < _pszEnd)
		{
			char c = *_pszCur;
			return c;
		}
		else
		{
			return '\0';
		}
	}
	void InitState(const char * pszBlob, size_t cchBlob);
	void HandleEndObject();
	void HandleEndArray();

	enum class ParseState
	{
		StartReadName,
		ReadName,
		ReadColon,
		StartReadValue,
		ReadStringValue,
		ReadIntValue,
		ReadObjectValue,
		ReadArrayValue,
		EndReadValue,
	};
	ParseState _State;

	struct ParseNode
	{
		ParseNode()
		{ }

		ParseNode(const std::string & name, NodeType type)
			: Name(name)
			, Type(type)
		{
		}

		std::string Name;
		NodeType Type { NodeType::None };
	};

	// stack of nodes 
	long m_cRef { 0 };
	std::vector<ParseNode> _ParseStack;
	const char * _pszBegin { nullptr };
	const char * _pszCur { nullptr };
	const char * _pszEnd { nullptr };

	NodeType _Type { NodeType::None };
    // todo: change to vector
	std::string _Name;
	std::string _Value;
	int64_t _IntValue{ 0 };
	bool _ReachedEnd{ false };
	bool _Skip{ false };
};

/***
	JsonReader provides a way to read json as sequence of items
	For object and array value, an app can either create a child reader object
	or skip over to the next item
*/
class JsonReader
{
public:
	using NodeType = JsonElementReader::NodeType;

	JsonReader(JsonStringSpan data);

	JsonReader(JsonReader&& rdr)
		: m_ElementRdr(rdr.m_ElementRdr)
		, m_InnerActive(rdr.m_InnerActive)
		, m_EndToken(rdr.m_EndToken)
	{
		m_pParent = rdr.m_pParent;
		rdr.m_pParent = nullptr;
	}

	~JsonReader();

	void Close()
	{
		if (m_pParent != nullptr)
		{
			m_pParent->SetInnerActive(false);
			m_pParent = nullptr;
		}
	}

	// return reader which can read the current node
	JsonReader ReadObject();
	JsonReader ReadArray();

	// read next element in node
	bool ReadNext();

	bool IsEnd() { return m_State == State::End; }
	NodeType Type() { return m_ElementRdr->Type(); }

	// return true if current token is EndObject or EndArray
	bool IsEndType()
	{
		return m_ElementRdr->Type() == NodeType::EndObject || m_ElementRdr->Type() == NodeType::EndArray;
	}
	bool IsString() { return m_ElementRdr->IsString(); }
	const std::string & Name() { return m_ElementRdr->Name(); }
	const std::string & Value() { return m_ElementRdr->Value(); }
	int IntValue() { return m_ElementRdr->IntValue(); }
	int64_t Int64Value() { return m_ElementRdr->Int64Value(); }
    
    // returns current position in stream
    const char* CurrentPos() { return m_ElementRdr->CurrentPos(); }

private:
	JsonReader(JsonReader* pParent, NodeType endToken);

	void SetInnerActive(bool val)
	{
		m_InnerActive = val;
	}
	
	void SetHaveToken()
	{
		m_State = State::HaveToken;
	}

private:
	enum class State
	{
		Start,
		Processing,
		// indicates that inner reader already read data and we should skip ReadNext call
		HaveToken,
		// set by ReadNext when ReadNext is used to skip over object or array
		EndRead,
		End,
	};

	JsonReader* m_pParent{ nullptr };
	JsonElementReader* m_ElementRdr { nullptr };
	NodeType m_EndToken{ NodeType::None };
	bool m_InnerActive{ false };

	// child reader reports end independently of actual stream state
	State m_State { State::Start };
};

}
