#include <cstdlib>
#include "jsonreader.h"

namespace Bondy {

///////////////////////////////////////////////////////////////////////////////
//
void JsonElementReader::InitState(const char * pszBlob, size_t cchBlob)
{
	_ReachedEnd = !(pszBlob != nullptr && cchBlob > 0);
	_Skip = false;
	_pszBegin = pszBlob;
	_pszEnd = pszBlob + cchBlob;
	_pszCur = _pszBegin;

	// json starts with object or array
	_State = ParseState::StartReadValue;
	_Name.resize(0);
	_Value.resize(0);
}

bool JsonElementReader::ReadNext()
{
	char c;

	if (_ReachedEnd)
	{
		return false;
	}

	_Name.resize(0);
	_Value.resize(0);

	for (;;)
	{
		c = PeekChar();
		if (c == '\0')
		{
			ReadChar();
			return false;
		}
		switch (_State)
		{
			case ParseState::StartReadName:
			{
				ReadChar();
				if (IsWhite(c))
				{
					;
				}
				else if (c == '"')
				{
					_State = ParseState::ReadName;
				}
				else if (c == '}')
				{
					// empty object
					HandleEndObject();
					return true;
				}
				else
				{
					Exception::Throw();
				}
			}
			break;
			case ParseState::ReadName:
			{
				ReadChar();
				// for now ignore escaping
				if (c != '"')
				{
					_Name.push_back(c);
				}
				else
				{
					_State = ParseState::ReadColon;
				}
			}
			break;
			case ParseState::ReadColon:
			{
				ReadChar();
				if (IsWhite(c))
				{
					;
				}
				else if (c == ':')
				{
					_State = ParseState::StartReadValue;
				}
				else
				{
					Exception::Throw();
				}
			}
			break;
			case ParseState::StartReadValue:
			{
				if (IsWhite(c))
				{
					ReadChar();
				}
				else if (c == '"')
				{
					ReadChar();
					_Type = NodeType::String;
					_State = ParseState::ReadStringValue;
				}
				else if (c == '[')
				{
					_Type = NodeType::Array;

					// start next iteration from reading values
					// do not consume [ until then
					_State = ParseState::ReadArrayValue;
					_ParseStack.push_back(ParseNode(_Name, NodeType::Array));
					return true;
				}
				else if (c == '{')
				{
					_Type = NodeType::Object;

					// start next iteration from reading name
					// do not consume [ until then
					_State = ParseState::ReadObjectValue;
					_ParseStack.push_back(ParseNode(_Name, NodeType::Object));
					return true;
				}
				else if ((c >= '0' && c <= '9') || c == '-')
				{
					ReadChar();
					_Type = NodeType::Int;
					_State = ParseState::ReadIntValue;

					_Value.push_back(c);
					if (!(PeekChar() >= '0' && PeekChar() <= '9'))
					{
						_IntValue = std::strtoll(&_Value[0], nullptr, 10);
						_State = ParseState::EndReadValue;
						return true;
					}
				}
				else if (c == ']')
				{
					ReadChar();
					// empty array
					HandleEndArray();
					return true;
				}
				else
				{
					Exception::Throw();
				}
			}
			break;
			case ParseState::ReadStringValue:
			{
				ReadChar();
				// for now ignore escaping
				if (c == '\\')
				{
					c = ReadUnescape();
					_Value.push_back(c);
				}
				else if (c != '"')
				{
					_Value.push_back(c);
				}
				else
				{
					_State = ParseState::EndReadValue;
					return true;
				}
			}
			break;
			case ParseState::ReadIntValue:
			{
				ReadChar();
				_Value.push_back(c);
				if (!(PeekChar() >= '0' && PeekChar() <= '9'))
				{
					_IntValue = std::strtoll(&_Value[0], nullptr, 10);
					_State = ParseState::EndReadValue;
					return true;
				}
			}
			break;
			case ParseState::ReadObjectValue:
			{
				// consume { char
				assert(c == '{');
				ReadChar();
				_State = ParseState::StartReadName;
			}
			break;
			case ParseState::ReadArrayValue:
			{
				// consume [ char
				assert(c == '[');
				ReadChar();
				_State = ParseState::StartReadValue;
			}
			break;
			case ParseState::EndReadValue:
			{
				ReadChar();
				if (IsWhite(c))
				{
					break;
				}
				else if (c == ',')
				{
					// if we inside array, keep reading values
					// otherwise read name
					if (_ParseStack.size() > 0 && _ParseStack.back().Type == NodeType::Array)
					{
						_State = ParseState::StartReadValue;
					}
					else
					{
						_State = ParseState::StartReadName;
					}

					break;
				}
				else if (c == '}')
				{
					// keep parse state as EndReadValue
					// on next iteration we might get another closure
					HandleEndObject();
					return true;
				}
				else if (c == ']')
				{
					// keep parse state as EndReadValue
					// on next iteration we might get another closure 
					HandleEndArray();
					return true;
				}
				else
				{
					Exception::Throw();
				}
			}
			break;
			default:
				Exception::Throw();
				break;
		}
	}

	return false;
}

void JsonElementReader::HandleEndObject()
{
	if (_ParseStack.size() == 0)
		Exception::Throw();

	_State = ParseState::EndReadValue;
	_Type = NodeType::EndObject;
	_Name = _ParseStack.back().Name;
	_ParseStack.resize(_ParseStack.size() - 1);
}

void JsonElementReader::HandleEndArray()
{
	if (_ParseStack.size() == 0)
		Exception::Throw();

	_State = ParseState::EndReadValue;
	_Type = NodeType::EndArray;
	_Name = _ParseStack.back().Name;
	_ParseStack.resize(_ParseStack.size() - 1);
}

///////////////////////////////////////////////////////////////////////////////
//
JsonReader::JsonReader(JsonStringSpan data)
{
	m_ElementRdr = new JsonElementReader(data);
	m_ElementRdr->AddRef();
}

JsonReader::JsonReader(JsonReader* pParent, NodeType endToken)
	: m_ElementRdr(pParent->m_ElementRdr)
	, m_pParent(pParent)
	, m_EndToken(endToken)
{
	m_ElementRdr->AddRef();

	if (m_pParent != nullptr)
	{
		m_pParent->SetInnerActive(true);
	}
}

JsonReader::~JsonReader()
{
	if (m_InnerActive)
	{
		// cannot remove outer object before inner is closed
		std::terminate();
	}

	Close();
	m_ElementRdr->Release();
	m_ElementRdr = nullptr;
}

// skips the content of current node
// read to next 
JsonReader JsonReader::ReadObject()
{
	if (m_InnerActive)
	{
		// app should not call read on parent object while child object is active
		std::terminate();
	}

	if (m_ElementRdr->Type() != NodeType::Object)
	{
		// incorrect object type for the call
		std::terminate();
	}

	return JsonReader(this, NodeType::EndObject);
}

JsonReader JsonReader::ReadArray()
{
	if (m_InnerActive)
	{
		// app should not call read on parent object while child object is active
		std::terminate();
	}

	if (m_ElementRdr->Type() != NodeType::Array)
	{
		// incorrect object type for the call
		std::terminate();
	}

	return JsonReader(this, NodeType::EndArray);
}

// read next element in node
bool JsonReader::ReadNext()
{
	if (m_InnerActive)
	{
		// app should not call read on parent object while child object is active
		std::terminate();
	}

	if (m_State == State::End)
	{
		return false;
	}

	switch (m_State)
	{
		case State::Start:
		{
			// if we are doing the first round (either with root reader or child, read the first element
			// for child reader we are consuming [ or { chars and moving to body
			m_State = State::Processing;
			if (!m_ElementRdr->ReadNext())
				Exception::Throw();

			// continue to the end; we might have finished the object
		}
		break;
		case State::HaveToken:
		{
			// if inner reader already reported token, skip reading
			m_State = State::Processing;

			// unless we are at the end token in which case we want to report end
			if (m_ElementRdr->Type() == NodeType::EndArray || m_ElementRdr->Type() == NodeType::EndObject)
			{
				// if we reached top most object, signal eos
				if (m_EndToken == NodeType::None)
				{
					m_State = State::End;
					m_ElementRdr->ReadNext();
					return false;
				}

				// when child returns, parent object might get to the end as well
				// but we should only see matching token type; otherwise it is failure
				if (m_EndToken != NodeType::None && m_ElementRdr->Type() != m_EndToken)
					Exception::Throw();
			}
		}
		break;
		case State::EndRead:
		{
			assert(m_ElementRdr->Type() == NodeType::EndObject || m_ElementRdr->Type() == NodeType::EndArray);
			m_State = State::Processing;
			return m_ElementRdr->ReadNext();
		}
		break;
		case State::Processing:
		{
			switch (m_ElementRdr->Type())
			{
				case NodeType::Int:
				case NodeType::String:
				{
					// if current element is not object, we can just read the next
					if (!m_ElementRdr->ReadNext())
						Exception::Throw();

					// continue to the end; we might have finished the object
				}
				break;
				case NodeType::Object:
				case NodeType::Array:
				{
					// if current element is object or array, skip until the end of element
					// by reading in skip mode
					NodeType startType = m_ElementRdr->Type();
					NodeType endType = (m_ElementRdr->Type() == NodeType::Array) ? NodeType::EndArray : NodeType::EndObject;

					// we already on the object. next read will start reading body so we set initial level to 1
					int level = 1;

					// skip until matching end object
					m_ElementRdr->SetSkipMode(true);

					while (m_ElementRdr->ReadNext())
					{
						if (m_ElementRdr->Type() == startType)
						{
							level++;
						}
						else if (m_ElementRdr->Type() == endType)
						{
							level--;
							if (level == 0)
							{
								break;
							}
						}
					}
					m_ElementRdr->SetSkipMode(false);
					if (level != 0)
						Bondy::Exception::Throw();

					// we now past the end of object
					if (m_ElementRdr->IsEnd())
					{
						m_State = State::End;
					}
					else
					{
						m_State = State::EndRead;
					}

					return !IsEnd();
				}
				break;
				case NodeType::EndObject:
				case NodeType::EndArray:
				{
					// if we reached top most object, signal eos
					if (m_EndToken == NodeType::None)
					{
						m_State = State::End;
						m_ElementRdr->ReadNext();
						return false;
					}

					// when child returns, parent object might get to the end as well
					// but we should only see matching token type; otherwise it is failure
					if (m_EndToken != NodeType::None && m_ElementRdr->Type() != m_EndToken)
						Exception::Throw();
				}
				break;
				default:
					Exception::Throw();
			}
			break;
		}
		default:
			Exception::Throw();
	}

	// if we reached our end token, return
	if (m_ElementRdr->Type() == m_EndToken)
	{
		if (m_pParent != nullptr)
			m_pParent->SetHaveToken();

		m_State = State::End;

		// consume end token so parent can continue with next state
		// we might have reached the end 
		m_ElementRdr->ReadNext();

		return false;
	}

	return true;
}

}

