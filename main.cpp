#include <iostream>
#include "Bondy.h"

class Type1Ptr;

class Type1
{
    template <typename U>
    friend class JsonSerializer;
    
public:
    using Obj = Type1;
    using Ptr = Type1Ptr;
    using IsPtr = std::false_type;

    uint32_t Field1;
    Bondy::Blob Blob;
    Bondy::StringView Str;
};

class Type1Ptr : public Bondy::BasePtr<Type1>
{
    template <typename U>
    friend class JsonSerializer;
public:
    using Obj = Type1;

    Type1Ptr(Bondy::ContextBase& ctx, Type1* data)
        : Bondy::BasePtr<Type1>(ctx, data)
    {}

	uint32_t Field1() const
	{
		return Ptr()->Field1;
	}

	void SetField1(uint32_t value)
	{
		Ptr()->Field1 = value;
	}

	Bondy::BlobPtr Blob() const
    {
        return Bondy::BlobPtr(*m_Ctx, Ptr()->Blob);
    }

    Bondy::StringPtr Str() const
    {
        return Bondy::StringPtr(*m_Ctx, Ptr()->Str);
    }
    
    void SetStr(Bondy::ConstStringView view)
    {
        Ptr()->Str = m_Ctx->AllocString(view).Data();
    }

    void SetStr(Bondy::StringPtr view)
    {
        Ptr()->Str = view.Data();
    }
    
    Type1& Data()
    {
        return *Ptr();
    }
};

template<>
struct Bondy::JsonSerializer<Type1, std::false_type>
{
public:
    static void Serialize(Bondy::ContextBase& ctx, JsonWriter& wrt, const Type1& val)
    {
        wrt.WriteObjectValueStart();
        wrt.WriteName(ConstStringView("Field1"));
        ::Bondy::JsonSerialize(ctx, wrt, val.Field1);
        wrt.WriteObjectValueEnd();
    }
    
    static void Deserialize(Bondy::ContextBase& ctx, JsonReader& rdr, Type1& val)
    {
		auto rdrObj = rdr.ReadObject();
		while (rdrObj.ReadNext())
		{
			if (rdrObj.IsEndType())
				continue;

            if(rdrObj.Name() == "Field1")
            {
                ::Bondy::JsonDeserialize(ctx, rdrObj, val.Field1);
            }
        }
    }
};

class Type3Ptr;
class Type3
{
    template <typename U>
    friend class JsonSerializer;
public:
    using Obj = Type3;
    using Ptr = Type3Ptr;
    using IsPtr = std::false_type;

    Type3()
    {
    }

	Type1 Field1;
	std::vector<Type1> VecType1;
	std::vector<Bondy::StringView> VecString;
};

class Type3Ptr : public Bondy::BasePtr<Type3>
{
    template <typename U>
    friend class JsonSerializer;
public:
    using Obj = Type3;
    
    Type3Ptr(Bondy::ContextBase& ctx, Type3* data)
        : Bondy::BasePtr<Type3>(ctx, data)
    {}

	Type1Ptr Field1()
	{
		return Type1Ptr(*m_Ctx, &Ptr()->Field1);
	}

	Bondy::VectorPtr<Type1> VecType1()
    {
        return Bondy::VectorPtr<Type1>(*m_Ctx, Ptr()->VecType1);
    }

	Bondy::VectorPtr<Bondy::StringView> VecString()
    {
        return Bondy::VectorPtr<Bondy::StringView>(*m_Ctx, Ptr()->VecString);
    }
};

template<>
struct Bondy::JsonSerializer<Type3, std::false_type>
{
    static void Serialize(Bondy::ContextBase& ctx, JsonWriter& wrt, const Type3& val)
    {
        wrt.WriteObjectValueStart();
        wrt.WriteName(ConstStringView("Field1"));
        ::Bondy::JsonSerialize(ctx, wrt, val.Field1);
        wrt.WriteArrayPairStart(ConstStringView("VecType1"));
        for(auto& f2 : val.VecType1)
        {
            ::Bondy::JsonSerialize(ctx, wrt, f2);
        }
        wrt.WriteArrayValueEnd();
        wrt.WriteObjectValueEnd();
    }
    
    static void Deserialize(Bondy::ContextBase& ctx, JsonReader& rdr, Type3& val)
    {
		if (rdr.Type() != JsonReader::NodeType::Object)
			Bondy::Exception::Throw();

		auto rdrObj = rdr.ReadObject();
		while(rdrObj.ReadNext())
        {
			if (rdrObj.IsEndType())
				continue;

            if(rdrObj.Name() == "Field1")
            {
                if (rdrObj.Type() != JsonReader::NodeType::Object)
                    throw Bondy::Exception();

                ::Bondy::JsonDeserialize(ctx, rdrObj, val.Field1);
            }
            else if(rdrObj.Name() == "VecType1")
            {
                if (rdrObj.Type() != JsonReader::NodeType::Array)
                    throw Bondy::Exception();
            }
        }
    }
};

class Type4Ptr;
class Type4
{
    template <typename U>
    friend class JsonSerializer;
public:
    using Obj = Type4;
    using Ptr = Type4Ptr;
    using IsPtr = std::false_type;

    Type4()
        : BondedField()
    {
    }

    Bondy::Bonded<Type3> BondedField;
};

class Type4Ptr : public Bondy::BasePtr<Type4>
{
    template <typename U>
    friend class JsonSerializer;
public:
    using Obj = Type4;
    
    Type4Ptr(Bondy::ContextBase& ctx, Type4* data)
        : Bondy::BasePtr<Type4>(ctx, data)
    {}

	Bondy::BondedPtr<Type3> BondedField()
    {
        return Bondy::BondedPtr<Type3>(*m_Ctx, &Ptr()->BondedField);
    }
};


template<>
struct Bondy::JsonSerializer<Type4, std::false_type>
{
    static void Serialize(Bondy::ContextBase& ctx, JsonWriter& wrt, const Type4& val)
    {
        wrt.WriteObjectValueStart();
        wrt.WriteName(ConstStringView("BondedField"));
        ::Bondy::JsonSerialize(ctx, wrt, val.BondedField);
        wrt.WriteObjectValueEnd();
    }
    
    static void Deserialize(Bondy::ContextBase& ctx, JsonReader& rdr, Type4& val)
    {
        if (rdr.Type() != JsonReader::NodeType::Object)
            Bondy::Exception::Throw();

        auto rdrObj = rdr.ReadObject();
        while(rdrObj.ReadNext())
        {
            if(rdrObj.Type() == JsonReader::NodeType::Object && rdrObj.Name() == "BondedField")
            {
                ::Bondy::JsonDeserialize(ctx, rdrObj, val.BondedField);
            }
        }
    }
};

struct JsonNode
{
	JsonNode(Bondy::JsonReader::NodeType type, bool next = false)
		: Type(type)
		, Next(next)
	{
	}

	JsonNode(int val)
		: Type(Bondy::JsonReader::NodeType::Int)
		, IntValue(val)
	{
	}

	JsonNode(const std::string& val)
		: Type(Bondy::JsonReader::NodeType::String)
		, StringValue(val)
	{
	}

	bool Match(Bondy::JsonReader::NodeType type) const
	{
		if (type == Type)
			return true;

		if (type == Bondy::JsonReader::NodeType::EndObject && Type == Bondy::JsonReader::NodeType::Object)
			return true;

		if (type == Bondy::JsonReader::NodeType::EndArray && Type == Bondy::JsonReader::NodeType::Array)
			return true;

		return false;
	}

	Bondy::JsonReader::NodeType Type;
	std::string Name;
	std::string StringValue;
	int IntValue { 0 };
	bool Next { false };
};

using JsonNodeIterator = std::initializer_list<JsonNode>::iterator;

void JsonIterate(Bondy::JsonReader& rdr, JsonNodeIterator& itCurrent, JsonNodeIterator itEnd)
{
	while (rdr.ReadNext())
	{
		if (itCurrent == itEnd)
			Bondy::Exception::Throw();

		if (!itCurrent->Match(rdr.Type()))
			Bondy::Exception::Throw();

		switch (rdr.Type())
		{
			case Bondy::JsonReader::NodeType::Object:
				{
					if (itCurrent->Next)
						break;
					itCurrent++;
					auto rdrChild = rdr.ReadObject();
					JsonIterate(rdrChild, itCurrent, itEnd);
				}
				break;
			case Bondy::JsonReader::NodeType::Array:
			{
				if (itCurrent->Next)
					break;
				itCurrent++;
				auto rdrChild = rdr.ReadArray();
				JsonIterate(rdrChild, itCurrent, itEnd);
			}
			break;
			case Bondy::JsonReader::NodeType::EndObject:
			case Bondy::JsonReader::NodeType::EndArray:
				if (!itCurrent->Next)
					Bondy::Exception::Throw();
				itCurrent++;
				break;
			case Bondy::JsonReader::NodeType::Int:
				if (rdr.IntValue() != itCurrent->IntValue)
					Bondy::Exception::Throw();
				itCurrent++;
				break;
			case Bondy::JsonReader::NodeType::String:
				if (rdr.Value() != itCurrent->StringValue)
					Bondy::Exception::Throw();
				itCurrent++;
				break;
			default:
				Bondy::Exception::Throw();
		}
	}
}

void JsonTestParse(const char* pszText, const std::initializer_list<JsonNode>& nodes)
{
	Bondy::JsonReader rdr(gsl::cstring_span<>(pszText, strlen(pszText)));
	auto itNodes = nodes.begin();
	JsonIterate(rdr, itNodes, nodes.end());
	if (itNodes != nodes.end())
		Bondy::Exception::Throw();
}

void JsonTest()
{
#if 0
	JsonTestParse("{}", { JsonNode(Bondy::JsonReader::NodeType::Object) });
	JsonTestParse("{\"field\" : {}}", { 
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(Bondy::JsonReader::NodeType::Object)});
	JsonTestParse("{\"field\":1}", {
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(1)});
	JsonTestParse("{\"field\":\"1\"}", { 
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode("1") });
	JsonTestParse("{\"field\":[1, 2]}", { 
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(Bondy::JsonReader::NodeType::Array),
		JsonNode(1),
		JsonNode(2)});
	JsonTestParse("{\"field\":{\"field2\" : 1}}", {
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(1) });
	JsonTestParse("[]", {
		JsonNode(Bondy::JsonReader::NodeType::Array) });
	JsonTestParse("[1, 2]", {
		JsonNode(Bondy::JsonReader::NodeType::Array),
		JsonNode(1),
		JsonNode(2) });
	JsonTestParse("[{\"field\":1}, {\"field\":2}]", {
		JsonNode(Bondy::JsonReader::NodeType::Array),
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(1),
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(2) });
#endif
	JsonTestParse("{\"field1\" : {\"field2\":1}, \"field3\" : [{\"field4\":2}]}", {
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(1),
		JsonNode(Bondy::JsonReader::NodeType::Array),
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(2) });
	// test skip logic
	JsonTestParse("{\"field1\" : {\"field2\":1}, \"field3\" : {\"field4\":2}}", {
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(Bondy::JsonReader::NodeType::Object, true),
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(2) });
	JsonTestParse("[{\"field\":1}, {\"field\":2}]", {
		JsonNode(Bondy::JsonReader::NodeType::Array),
		JsonNode(Bondy::JsonReader::NodeType::Object, true),
		JsonNode(Bondy::JsonReader::NodeType::Object),
		JsonNode(2) });
	JsonTestParse("[[1], [2, 3]]", {
		JsonNode(Bondy::JsonReader::NodeType::Array),
		JsonNode(Bondy::JsonReader::NodeType::Array, true),
		JsonNode(Bondy::JsonReader::NodeType::Array),
		JsonNode(2),
		JsonNode(3) });
}

int main(int argc, const char * argv[])
{
	JsonTest();
    Bondy::Context<Type4> ctx;
    Type4Ptr t4 = ctx.Root();
    
    // context can be same or different
    // app is responsible for keeping context alive
    Bondy::StackObj<Type3> t3(ctx);
    t3.VecString().push_back(ctx.AllocString("Hello"));

	t3.Field1().SetField1(3);

    Bondy::StackObj<Type1> t1(ctx);
	t1.SetField1(2);
    t1.SetStr("Welcome");
    t3.VecType1().push_back(t1.Data());
    
    t4.BondedField().Serialize(t3);
    
    Bondy::JsonWriter wrt;
    Bondy::JsonSerialize(ctx, wrt, t4);
    
    Bondy::StackObj<Type4> t4p(ctx);
    Bondy::JsonDeserialize(ctx, t4p, wrt.TakeResult());
    Bondy::StackObj<Type3> t3p(ctx);
    t4p.BondedField().Deserialize(t3p);

	assert(t3p.Field1().Field1() == 3);
	assert(t3p.VecType1().size() == 1);
    
    return 0;
}


